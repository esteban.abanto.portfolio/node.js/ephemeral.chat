# Ephemeral Chat

This project is a real-time chat with ephemeral characteristics, meaning that messages are lost upon closing or refreshing the page. The chat operates entirely within the client's memory, and messages are not stored in a database or any other persistent storage. Messages are added to a Redux object on the client and are reset when the page is refreshed. Additionally, users have the ability to modify their names in the chat. Private chats are also supported in addition to the global chat.

## Technologies Used

### Backend
- Node.js
- Express
- Socket.io
- TypeScript

### Frontend
- React
- Redux
- Socket.io-client
- TypeScript

## Try the Chat
You can try the chat [here](https://ephemeral-chat-z2zs.onrender.com).

## Guide to Using Tools

### Customizing Your Chat Name
The username is automatically generated upon accessing the page, following the structure "User_" followed by a randomly generated four-digit number. This practice was implemented so that users do not have to think about a name when starting to use the global chat. This randomly generated name is what appears in all messages sent by the client and is what other users will see as the message sender.

![Default Frame Name](assets/images/default-frame-name.jpg)

However, users have the option to personalize their username. By clicking on the text input box, they can enter the desired name, with a maximum limit of 20 characters. This restriction was set to ensure that the text in the messages does not deform. Once the client has updated their name, they simply need to press the "Enter" key or click on the "Save" button next to the text input box to confirm the change.

### Using Private Chats

To initiate a private chat with a specific user, the user must first have written something in the global chat. This action causes their message and username to appear in the global chat. 

![Initial Message](assets/images/initial-message.jpg)

When this occurs, you can click on the user's name, which will create a new chat in the chat list section where you can send them a completely private message.

![Initial Message](assets/images/show-private-chat.jpg)

Only when you send a message to the other user will the private chat be created for them, allowing both parties to engage in private conversation.

## Project Structure
The project consists of two main folders:

- **backend**: This folder contains the server implementation using Node.js, Express, Socket.io, and TypeScript. The server is configured to run on port 4000.

- **frontend**: Here you will find the client application developed with React, TypeScript, Redux, and Socket.io-client. The client application is hosted on port 3000.

Both parts of the project work independently but are used together.

## Installation and Usage

### Clone this repository
```bash
git clone https://gitlab.com/esteban.abanto.portfolio/node.js/ephemeral.chat.git
```

### Backend

1. Install backend dependencies:
```bash
cd backend
npm install
```

2. Start the backend server:
```bash
npm run dev
```

### Frontend

1. Install frontend dependencies:
```bash
cd frontend
npm install
```

2. Start the frontend application:
```bash
npm start
```

## Feedback and Suggestions

Your feedback is invaluable in helping me improve this project! If you have any suggestions or ideas to share, please don't hesitate to reach out via email at [esteban.abanto.2709@gmail.com](mailto:esteban.abanto.2709@gmail.com?subject=Suggestion%20for%20Ephemeral%20Chat)
. Your input is highly appreciated!

## License
This project is licensed under the [BSD 3-Clause License](https://opensource.org/licenses/BSD-3-Clause).